﻿// <copyright file="GameRenderFramework.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.View
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using Model;
    using VM;

    /// <summary>
    /// Rendes the game
    /// </summary>
    public class GameRenderFramework : FrameworkElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderFramework"/> class.
        /// </summary>
        public GameRenderFramework()
        {
        }

        /// <summary>
        /// Gets or sets the viewmodel
        /// </summary>
        public ViewModel VM { get; set; }

        /// <summary>
        /// Gets or sets the rendeders viewmode
        /// </summary>
        public ViewModes ViewMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether player clicked on cowgirl button
        /// </summary>
        public bool ClickedOnCowgirl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether player clicked on cactusirl button
        /// </summary>
        public bool ClickedOnCactus { get; set; }

        /// <summary>
        /// Draw the screen
        /// </summary>
        /// <param name="drawingContext">drawingContext</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.VM == null)
            {
                return;
            }
            else if (this.ViewMode == ViewModes.MainMenuViewMode)
            {
                this.DrawMainmenu(drawingContext);
            }
            else if (this.ViewMode == ViewModes.HighScoreViewMode)
            {
                this.DrawHighScores(drawingContext);
            }
            else if (this.ViewMode == ViewModes.RulesViewMode)
            {
                this.DrawRules(drawingContext);
            }
            else if (this.ViewMode == ViewModes.GameViewMode)
            {
                this.DrawGame(drawingContext);
            }
            else if (this.ViewMode == ViewModes.PlayerWinAnimationView)
            {
                this.VM.PlayerWinAnim.Play(drawingContext);
            }
            else if (this.ViewMode == ViewModes.LevelStartAnimationView)
            {
                this.VM.LevelStartAnim.Play(drawingContext);
            }
        }

        private void DrawGame(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(this.VM.ImageContainer.MapBackground, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));

            foreach (var character in this.VM.GameLogic.Attackers)
            {
                drawingContext.DrawGeometry(character.GetBrush(), null, character.GeometryObject);
            }

            foreach (var character in this.VM.GameLogic.Defenders)
            {
                drawingContext.DrawGeometry(character.GetBrush(), null, character.GeometryObject);
            }

            this.DrawLevelText(drawingContext);

            if (this.ClickedOnCowgirl)
            {
                drawingContext.DrawRectangle(null, this.VM.GameLogic.MapModel.Frame, this.VM.GameLogic.MapModel.ClickedCowgirlPlace);
            }

            if (this.ClickedOnCactus)
            {
                drawingContext.DrawRectangle(null, this.VM.GameLogic.MapModel.Frame, this.VM.GameLogic.MapModel.ClickedCactusPlace);
            }
        }

        private void DrawLevelText(DrawingContext drawingContext)
        {
            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                this.VM.GameLogic.Money.ToString() + "$", 20, this.VM.ImageContainer.ZombieLightBrown),
                this.VM.GameLogic.MapModel.MoneyPlace);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                this.VM.GameLogic.Score.ToString() + " Pt", 20, this.VM.ImageContainer.ZombieLightBrown),
                this.VM.GameLogic.MapModel.ScorePlace);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "Level " + this.VM.GameLogic.ActualLevel.ToString(), 20, this.VM.ImageContainer.ZombieLightBrown),
                this.VM.GameLogic.MapModel.LevelPlace);
        }

        private void DrawMainmenu(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(this.VM.ImageContainer.MenuBackground, null, this.VM.MenuModel.MenuBackground);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "Cowgirls vs Zombies",
                48,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.MainTitleTextPoint);

            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.NewGameButton);
            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.HighScoreButton);
            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.RulesButton);
            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.ExitButton);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                    "New Game",
                    28,
                    this.VM.ImageContainer.ZombieLightBrown),
                    this.VM.MenuModel.NewGameTextPoint);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                    "HighScores",
                    28,
                    this.VM.ImageContainer.ZombieLightBrown),
                    this.VM.MenuModel.HighScoreTextPoint);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                    "Rules",
                    28,
                    this.VM.ImageContainer.ZombieLightBrown),
                    this.VM.MenuModel.RulesTextPoint);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                    "Exit",
                    28,
                    this.VM.ImageContainer.ZombieLightBrown),
                    this.VM.MenuModel.ExitTextPoint);
        }

        private void DrawHighScores(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(this.VM.ImageContainer.MenuBackground, null, this.VM.MenuModel.MenuBackground);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "High Scores",
                48,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.HighScoreTitleTextPoint);

            drawingContext.DrawRectangle(this.VM.ImageContainer.MenuOpaWhite, null, this.VM.MenuModel.HighScoreListBackground);

            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.HighScoreExitButton);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "Back",
                28,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.HighScoreExitTextPoint);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                this.VM.MenuModel.HighScores,
                20,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.HighScoreListTextPoint);
        }

        private void DrawRules(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(this.VM.ImageContainer.MenuBackground, null, this.VM.MenuModel.MenuBackground);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "Rules",
                48,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.RulesTitleTextPoint);

            drawingContext.DrawRectangle(this.VM.ImageContainer.MenuOpaWhite, null, this.VM.MenuModel.HighScoreListBackground);

            drawingContext.DrawRectangle(this.VM.ImageContainer.ZombieDarkBrown, this.VM.ImageContainer.MenuPen, this.VM.MenuModel.HighScoreExitButton);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                "Back",
                28,
                this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.HighScoreExitTextPoint);

            drawingContext.DrawText(
                this.VM.ImageContainer.GetGameText(
                    this.VM.MenuModel.GameRules,
                    20,
                    this.VM.ImageContainer.ZombieLightBrown),
                this.VM.MenuModel.HighScoreListTextPoint);
        }
    }
}
