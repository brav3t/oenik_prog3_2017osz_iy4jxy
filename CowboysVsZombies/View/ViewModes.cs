﻿// <copyright file="ViewModes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// render mode
    /// </summary>
    public enum ViewModes
    {
        /// <summary>
        /// renders the main menu
        /// </summary>
        MainMenuViewMode,

        /// <summary>
        /// renders the high score menu
        /// </summary>
        HighScoreViewMode,

        /// <summary>
        /// renders the rules menu
        /// </summary>
        RulesViewMode,

        /// <summary>
        /// renders the game
        /// </summary>
        GameViewMode,

        /// <summary>
        /// Shows player wins animation
        /// </summary>
        PlayerWinAnimationView,

        /// <summary>
        /// When level start plays
        /// </summary>
        LevelStartAnimationView,

        /// <summary>
        /// When level completed
        /// </summary>
        LevelEndAnimationView
    }
}
