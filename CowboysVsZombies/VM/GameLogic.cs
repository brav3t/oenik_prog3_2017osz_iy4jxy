﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Model;

    /// <summary>
    /// Describes the game's logic
    /// </summary>
    public class GameLogic
    {
        private static Random rnd;
        private int gameTurnCount;
        private int maxZombie;
        private int createdZombieCount;
        private int killedZombieCount;
        private int randomHigh;
        private int randomLow;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        public GameLogic()
        {
            this.GameInitialized = false;
            rnd = new Random();
            this.MapModel = new MapModel();
            this.Attackers = new List<Character>();
            this.Defenders = new List<Character>();
            this.ActualLevel = 1;

            // Events
            this.LevelComplete += this.GameLogic_LevelComplete;
            this.EndGame += this.GameLogic_EndGame;
        }

        /// <summary>
        /// if level starts
        /// </summary>
        public event EventHandler LevelStart;

        /// <summary>
        /// if level 1 complete
        /// </summary>
        public event EventHandler<LevelEndArgs> LevelComplete;

        /// <summary>
        /// if game ends
        /// </summary>
        public event EventHandler<GameEndArgs> EndGame;

        /// <summary>
        /// Gets Mapmodel
        /// </summary>
        public MapModel MapModel { get; }

        /// <summary>
        /// Gets the attacker zombies
        /// </summary>
        public List<Character> Attackers { get; private set; }

        /// <summary>
        /// Gets the defenders aka cowgirls and cactus
        /// </summary>
        public List<Character> Defenders { get; private set; }

        /// <summary>
        /// Gets players money
        /// </summary>
        public int Money { get; private set; }

        /// <summary>
        /// Gets players score
        /// </summary>
        public int Score { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether game is initialized
        /// </summary>
        public bool GameInitialized { get; set; }

        /// <summary>
        /// Gets or sets actual level
        /// </summary>
        public int ActualLevel { get; set; }

        /// <summary>
        /// One turn per tick
        /// </summary>
        public void GameTurn()
        {
            int zombieSpawnTime = rnd.Next(this.randomLow, this.randomHigh);

            if (this.gameTurnCount > zombieSpawnTime && this.createdZombieCount < this.maxZombie)
            {
                this.CreateZombie();
                this.createdZombieCount++;
                this.gameTurnCount = 0;
            }

            // Handles empty lists
            try
            {
                foreach (var zombie in this.Attackers)
                {
                    foreach (var defender in this.Defenders)
                    {
                        if (defender.HealthPoint <= 0)
                        {
                            this.Defenders.Remove(defender);
                            defender.CharacterField.FieldIsAvailable = true;
                        }
                        else
                        {
                            if (zombie.Collide(defender))
                            {
                                zombie.CharState = CharacterStates.Attacking;
                                zombie.Target = defender;
                            }
                        }
                    }

                    zombie.Act();

                    if (zombie.Collide(this.MapModel.TheSaloon))
                    {
                        this.EndGame?.Invoke(this, new GameEndArgs("zombies"));
                    }
                }

                foreach (var defender in this.Defenders)
                {
                    foreach (var zombie in this.Attackers)
                    {
                        if (zombie.HealthPoint <= 0)
                        {
                            this.Attackers.Remove(zombie);
                            this.Money += 100;
                            this.killedZombieCount++;
                            if (this.killedZombieCount == this.maxZombie)
                            {
                                this.LevelComplete?.Invoke(this, new LevelEndArgs(this.ActualLevel));
                            }
                        }
                        else
                        {
                            if (defender is CowGirl &&
                                defender.Target == null &&
                                zombie.GeometryObject.Bounds.Location.Y.Equals(defender.GeometryObject.Bounds.Location.Y))
                            {
                                defender.CharState = CharacterStates.Attacking;
                                defender.Target = zombie;
                            }

                            if (defender is Cactus && defender.Collide(zombie))
                            {
                                defender.CharState = CharacterStates.Attacking;
                                defender.Target = zombie;
                            }
                        }
                    }

                    defender.Act();
                }
            }
            catch
            {
            }

            this.gameTurnCount++;
        }

        /// <summary>
        /// Initialize the game
        /// </summary>
        public void InitLevel1()
        {
            this.ActualLevel = 1;
            this.Score = 0;
            this.LevelStart?.Invoke(this, new EventArgs());

            this.MapModel.ResetFields();
            this.Attackers.Clear();
            this.Defenders.Clear();

            this.gameTurnCount = 0;
            this.createdZombieCount = 0;
            this.maxZombie = 10;
            this.killedZombieCount = 0;
            this.Money = 500;
            this.randomLow = 100;
            this.randomHigh = 200;

            this.GameInitialized = true;
        }

        /// <summary>
        /// Initialize level 2
        /// </summary>
        public void InitLevel2()
        {
            // this.ActualLevel = 2;
            this.LevelStart?.Invoke(this, new EventArgs());

            this.MapModel.ResetFields();
            this.Attackers.Clear();
            this.Defenders.Clear();

            this.gameTurnCount = 0;
            this.createdZombieCount = 0;
            this.maxZombie = 30;
            this.killedZombieCount = 0;
            this.Money = 400;
            this.randomLow = 50;
            this.randomHigh = 150;

            this.GameInitialized = true;
        }

        /// <summary>
        /// Initialize level 3
        /// </summary>
        public void InitLevel3()
        {
            // this.ActualLevel = 3;
            this.LevelStart?.Invoke(this, new EventArgs());

            this.MapModel.ResetFields();
            this.Attackers.Clear();
            this.Defenders.Clear();

            this.gameTurnCount = 0;
            this.createdZombieCount = 0;
            this.maxZombie = 50;
            this.killedZombieCount = 0;
            this.Money = 600;
            this.randomLow = 25;
            this.randomHigh = 100;

            this.GameInitialized = true;
        }

        /// <summary>
        /// places the charater
        /// </summary>
        /// <param name="characterType">placeable characters type</param>
        /// <param name="click">where was clicked</param>
        public void PlaceCharacter(CharacterTypes characterType, Point click)
        {
            MapField availableField = null;

            try
            {
                availableField = this.WhereToPlaceCharacter(click);
            }
            catch
            {
            }

            if (availableField != null)
            {
                if (characterType.Equals(CharacterTypes.CowGirl))
                {
                    this.CreateCowGirl(availableField);
                }
                else if (characterType.Equals(CharacterTypes.Cactus))
                {
                    this.CreateCactus(availableField);
                }
            }
        }

        /// <summary>
        /// Creates a zombie and adds it to the horde
        /// </summary>
        public void CreateZombie()
        {
            int line = rnd.Next(4);

            Point charLocation = new Point(650, 105 + (100 * line));
            Size charSize = new Size(90, 90);
            Rect charRect = new Rect(charLocation, charSize);
            RectangleGeometry characterGeometry = new RectangleGeometry(charRect);
            Zombie zombie = new Zombie(characterGeometry, CharacterStates.Moving, 200, 10);
            this.Attackers.Add(zombie);
        }

        /// <summary>
        /// Creates a cowgirl and adds it to the defenders
        /// </summary>
        /// <param name="availableField">where to place the girl</param>
        public void CreateCowGirl(MapField availableField)
        {
            if (this.Money >= 200)
            {
                this.Money -= 200;

                Point charLocation = availableField.PlacePoint;
                Size charSize = new Size(90, 90);
                Rect charRect = new Rect(charLocation, charSize);
                RectangleGeometry characterGeometry = new RectangleGeometry(charRect);
                CowGirl cowgirl = new CowGirl(characterGeometry, CharacterStates.Idle, 50, 1);
                cowgirl.CharacterField = availableField;
                cowgirl.CharacterField.FieldIsAvailable = false;
                this.Defenders.Add(cowgirl);
            }
        }

        /// <summary>
        /// Creates a cactus and adds it to the defenders
        /// </summary>
        /// <param name="availableField">where to place the girl</param>
        public void CreateCactus(MapField availableField)
        {
            if (this.Money >= 50)
            {
                this.Money -= 50;

                Point charLocation = availableField.PlacePoint;
                Size charSize = new Size(90, 90);
                Rect charRect = new Rect(charLocation, charSize);
                RectangleGeometry characterGeometry = new RectangleGeometry(charRect);
                Cactus cactus = new Cactus(characterGeometry, CharacterStates.Idle, 200, 5);
                cactus.CharacterField = availableField;
                cactus.CharacterField.FieldIsAvailable = false;
                this.Defenders.Add(cactus);
            }
        }

        /// <summary>
        /// Gets the location of the clicked field
        /// </summary>
        /// <param name="click">where was the click</param>
        /// <returns>location of the field</returns>
        private MapField WhereToPlaceCharacter(Point click)
        {
            if (click.X >= 50 && click.X < 150)
            {
                if (click.Y >= 100 && click.Y < 200)
                {
                    if (this.MapModel.Fields[0].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[0];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 200 && click.Y < 300)
                {
                    if (this.MapModel.Fields[1].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[1];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 300 && click.Y < 400)
                {
                    if (this.MapModel.Fields[2].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[2];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 400 && click.Y < 500)
                {
                    if (this.MapModel.Fields[3].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[3];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (click.X >= 150 && click.X < 250)
            {
                if (click.Y >= 100 && click.Y < 200)
                {
                    if (this.MapModel.Fields[4].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[4];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 200 && click.Y < 300)
                {
                    if (this.MapModel.Fields[5].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[5];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 300 && click.Y < 400)
                {
                    if (this.MapModel.Fields[6].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[6];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 400 && click.Y < 500)
                {
                    if (this.MapModel.Fields[7].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[7];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (click.X >= 250 && click.X < 350)
            {
                if (click.Y >= 100 && click.Y < 200)
                {
                    if (this.MapModel.Fields[8].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[8];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 200 && click.Y < 300)
                {
                    if (this.MapModel.Fields[9].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[9];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 300 && click.Y < 400)
                {
                    if (this.MapModel.Fields[10].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[10];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 400 && click.Y < 500)
                {
                    if (this.MapModel.Fields[11].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[11];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (click.X >= 350 && click.X < 450)
            {
                if (click.Y >= 100 && click.Y < 200)
                {
                    if (this.MapModel.Fields[12].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[12];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 200 && click.Y < 300)
                {
                    if (this.MapModel.Fields[13].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[13];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 300 && click.Y < 400)
                {
                    if (this.MapModel.Fields[14].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[14];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 400 && click.Y < 500)
                {
                    if (this.MapModel.Fields[15].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[15];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (click.X >= 450 && click.X < 550)
            {
                if (click.Y >= 100 && click.Y < 200)
                {
                    if (this.MapModel.Fields[16].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[16];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 200 && click.Y < 300)
                {
                    if (this.MapModel.Fields[17].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[17];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 300 && click.Y < 400)
                {
                    if (this.MapModel.Fields[18].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[18];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else if (click.Y >= 400 && click.Y < 500)
                {
                    if (this.MapModel.Fields[19].FieldIsAvailable)
                    {
                        return this.MapModel.Fields[19];
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            else
            {
                throw new Exception();
            }
        }

        private void GameLogic_EndGame(object sender, GameEndArgs e)
        {
            this.GameInitialized = false;
            this.ActualLevel = 1;
        }

        private void GameLogic_LevelComplete(object sender, LevelEndArgs e)
        {
            this.Score += this.Money;
            this.GameInitialized = false;

            this.ActualLevel += 1;
            this.NextLevel();
        }

        private void NextLevel()
        {
            switch (this.ActualLevel)
            {
                case 1:
                    this.InitLevel1();
                    break;
                case 2:
                    this.InitLevel2();
                    break;
                case 3:
                    this.InitLevel3();
                    break;
                case 4:
                    this.EndGame?.Invoke(this, new GameEndArgs("player"));
                    break;
                default:
                    break;
            }
        }
    }
}
