﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using Model;
    using Model.Animation;
    using Model.Menu;
    using View;

    /// <summary>
    /// The ViewModel
    /// </summary>
    public class ViewModel
    {
        private GameRenderFramework gameRenderFramework;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        /// <param name="gameRenderFramework">the gamerenderframework</param>
        public ViewModel(GameRenderFramework gameRenderFramework)
        {
            // Models
            this.ImageContainer = new ImageContainer();
            this.MenuModel = new MenuModel();
            this.GameLogic = new GameLogic();
            this.gameRenderFramework = gameRenderFramework;
            this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;

            // Animations
            this.PlayerWinAnim = new PlayerWinAnimation(this.ImageContainer);
            this.LevelStartAnim = new LevelStartAnimation(this.ImageContainer, this.GameLogic);

            // Events
            this.GameLogic.LevelComplete += this.GameLogic_LevelComplete;
            this.GameLogic.EndGame += this.GameLogic_EndGame;
            this.PlayerWinAnim.AnimationEnded += this.PlayerWinAnim_AnimationEnded;
            this.GameLogic.LevelStart += this.GameLogic_LevelStart;
            this.LevelStartAnim.AnimationEnded += this.LevelStartAnim_AnimationEnded;
        }

        /// <summary>
        /// Gets ImageContainer
        /// </summary>
        public ImageContainer ImageContainer { get; }

        /// <summary>
        /// Gets MenuModel
        /// </summary>
        public MenuModel MenuModel { get; private set; }

        /// <summary>
        /// Gets Gamelogic
        /// </summary>
        public GameLogic GameLogic { get; private set; }

        /// <summary>
        /// Gets PlayerWinAnimation
        /// </summary>
        public PlayerWinAnimation PlayerWinAnim { get; }

        /// <summary>
        /// Gets level start animation
        /// </summary>
        public LevelStartAnimation LevelStartAnim { get; }

        /// <summary>
        /// If left  mouse button is clicked on mainwindows
        /// </summary>
        /// <param name="x">click x point</param>
        /// <param name="y">click y point</param>
        public void MouseButtonDown(int x, int y)
        {
            if (this.gameRenderFramework.ViewMode == ViewModes.MainMenuViewMode)
            {
                // Clicked on new game (200 x 50)
                if (x >= 225 && x <= 425 && y >= 130 && y <= 180)
                {
                    this.GameLogic.InitLevel1();
                }

                // Clicked on high score
                if (x >= 225 && x <= 425 && y >= 205 && y <= 285)
                {
                    this.gameRenderFramework.ViewMode = ViewModes.HighScoreViewMode;
                }

                // Clicked Rules
                if (x >= 225 && x <= 425 && y >= 280 && y <= 360)
                {
                    this.gameRenderFramework.ViewMode = ViewModes.RulesViewMode;
                }
            }
            else if (this.gameRenderFramework.ViewMode == ViewModes.HighScoreViewMode)
            {
                if (x >= 225 && x <= 425 && y >= 425 && y <= 475)
                {
                    this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;
                }
            }
            else if (this.gameRenderFramework.ViewMode == ViewModes.RulesViewMode)
            {
                if (x >= 225 && x <= 425 && y >= 425 && y <= 475)
                {
                    this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;
                }
            }
            else if (this.gameRenderFramework.ViewMode == ViewModes.LevelStartAnimationView)
            {
                if (x > 15 && x < 35 && y > 465 && y < 485)
                {
                    this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;
                    this.LevelStartAnim.TickCounter = 0;
                }
            }
            else if (this.gameRenderFramework.ViewMode == ViewModes.GameViewMode)
            {
                if (this.gameRenderFramework.ClickedOnCowgirl)
                {
                    this.GameLogic.PlaceCharacter(CharacterTypes.CowGirl, new Point(x, y));
                    this.gameRenderFramework.ClickedOnCowgirl = false;
                }
                else if (this.gameRenderFramework.ClickedOnCactus)
                {
                    this.GameLogic.PlaceCharacter(CharacterTypes.Cactus, new Point(x, y));
                    this.gameRenderFramework.ClickedOnCactus = false;
                }
                else
                {
                    if (x <= 100 && y <= 100)
                    {
                        this.gameRenderFramework.ClickedOnCowgirl = true;
                    }

                    if (x > 100 && x <= 200 && y <= 100)
                    {
                        this.gameRenderFramework.ClickedOnCactus = true;
                    }

                    if (x > 525 && x < 543 && y > 40 && y < 64)
                    {
                        this.GameLogic.CreateZombie();
                    }

                    if (x > 15 && x < 35 && y > 465 && y < 485)
                    {
                        this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;
                        this.GameLogic.ActualLevel = 1;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the vm when ticking
        /// </summary>
        public void VMUpdate()
        {
            if (this.gameRenderFramework.ViewMode == ViewModes.GameViewMode && this.GameLogic.GameInitialized)
            {
                this.GameLogic.GameTurn();
                this.gameRenderFramework.InvalidateVisual();
            }
            else
            {
                this.gameRenderFramework.InvalidateVisual();
            }
        }

        private void GameLogic_EndGame(object sender, GameEndArgs e)
        {
            if (e.Who.Equals("zombies"))
            {
                this.gameRenderFramework.ViewMode = ViewModes.MainMenuViewMode;
            }
            else if (e.Who.Equals("player"))
            {
                this.gameRenderFramework.ViewMode = ViewModes.PlayerWinAnimationView;
            }
        }

        private void GameLogic_LevelComplete(object sender, LevelEndArgs e)
        {
            throw new NotImplementedException();
        }

        private void LevelStartAnim_AnimationEnded(object sender, EventArgs e)
        {
            this.gameRenderFramework.ViewMode = ViewModes.GameViewMode;
        }

        /// <summary>
        /// If animation ends save the game
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void PlayerWinAnim_AnimationEnded(object sender, EventArgs e)
        {
            if (this.GameLogic.Score >= this.MenuModel.LowestHighScore || this.MenuModel.HighScoreListCount < 13)
            {
                this.MenuModel.ActualScore = this.GameLogic.Score;

                GetPlayerName getPlayerName = new GetPlayerName(this.MenuModel, this.GameLogic.Score);
                getPlayerName.Show();
            }

            this.gameRenderFramework.ViewMode = ViewModes.HighScoreViewMode;
        }

        private void GameLogic_LevelStart(object sender, EventArgs e)
        {
            this.gameRenderFramework.ViewMode = ViewModes.LevelStartAnimationView;
        }
    }
}
