﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

// Game images from: https://gameartpartners.com/ Thanks!
namespace CowboysVsZombies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;
    using Model.Menu;
    using VM;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel vm;

        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            this.vm = new ViewModel(this.GameRenderer);

            this.Loaded += this.MainWindow_Loaded;
            this.MouseLeftButtonDown += this.MainWindow_MouseLeftButtonDown;
        }

        private void MainWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            double x = e.GetPosition(this).X;
            double y = e.GetPosition(this).Y;

            // Clicked on exit
            if (x >= 225 && x <= 425 && y >= 355 && y <= 435 && this.GameRenderer.ViewMode == View.ViewModes.MainMenuViewMode)
            {
                this.Close();
            }
            else
            {
                this.vm.MouseButtonDown((int)x, (int)y);
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.GameRenderer.VM = this.vm;

            this.timer = new DispatcherTimer();
            this.timer.Interval = new TimeSpan(0, 0, 0, 0, 20);
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.vm.VMUpdate();
        }

        private void GameRenderer_ExitGame(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
