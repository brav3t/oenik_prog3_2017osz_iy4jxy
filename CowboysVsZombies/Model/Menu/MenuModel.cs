﻿// <copyright file="MenuModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Windows;
    using Menu;
    using View;

    /// <summary>
    /// Containt menu elements and information
    /// </summary>
    public class MenuModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuModel"/> class.
        /// </summary>
        public MenuModel()
        {
            this.LoadHighScores();
            this.LoadRules();

            // Points
            Point origo = new Point(0, 0);

            Point newGameButtonPlace = new Point(225, 130);
            Point highScoreButtonPlace = new Point(225, 205);
            Point rulesButtonPlace = new Point(225, 280);
            Point exitButtonPlace = new Point(225, 355);
            Point highScoreListBackgroundPlace = new Point(100, 100);
            Point highScoreExitButtonPlace = new Point(225, 425);
            this.MainTitleTextPoint = new Point(40, 25);
            this.NewGameTextPoint = new Point(243, 138);
            this.RulesTextPoint = new Point(278, 288);
            this.ExitTextPoint = new Point(295, 363);
            this.HighScoreTextPoint = new Point(231, 213);
            this.HighScoreExitTextPoint = new Point(290, 433);
            this.HighScoreTitleTextPoint = new Point(170, 25);
            this.HighScoreListTextPoint = new Point(115, 105);
            this.RulesTitleTextPoint = new Point(250, 25);

            // Sizes
            Size menuButtonSize = new Size(200, 50);
            Size menuBackgroundSize = new Size(650, 500);
            Size highScoreListBackgroundSize = new Size(450, 300);

            // Rects
            this.MenuBackground = new Rect(origo, menuBackgroundSize);
            this.NewGameButton = new Rect(newGameButtonPlace, menuButtonSize);
            this.HighScoreButton = new Rect(highScoreButtonPlace, menuButtonSize);
            this.RulesButton = new Rect(rulesButtonPlace, menuButtonSize);
            this.ExitButton = new Rect(exitButtonPlace, menuButtonSize);
            this.HighScoreListBackground = new Rect(highScoreListBackgroundPlace, highScoreListBackgroundSize);
            this.HighScoreExitButton = new Rect(highScoreExitButtonPlace, menuButtonSize);
        }

        /// <summary>
        /// Gets game title point
        /// </summary>
        public Point MainTitleTextPoint { get; }

        /// <summary>
        /// Gets new game text point
        /// </summary>
        public Point NewGameTextPoint { get; }

        /// <summary>
        /// Gets highscoretext point
        /// </summary>
        public Point HighScoreTextPoint { get; }

        /// <summary>
        /// Gets roles text point
        /// </summary>
        public Point RulesTextPoint { get; }

        /// <summary>
        /// Gets exit text point
        /// </summary>
        public Point ExitTextPoint { get; }

        /// <summary>
        /// Gets high score exit button text point
        /// </summary>
        public Point HighScoreExitTextPoint { get; }

        /// <summary>
        /// Gets high score exit button text point
        /// </summary>
        public Point HighScoreTitleTextPoint { get; }

        /// <summary>
        /// Gets rules title place
        /// </summary>
        public Point RulesTitleTextPoint { get; }

        /// <summary>
        /// Gets high score list text place
        /// </summary>
        public Point HighScoreListTextPoint { get; }

        // Rects

        /// <summary>
        /// Gets main menu rect
        /// </summary>
        public Rect MenuBackground { get; }

        /// <summary>
        /// Gets new game button rect
        /// </summary>
        public Rect NewGameButton { get; }

        /// <summary>
        /// Gets highscore button rect
        /// </summary>
        public Rect HighScoreButton { get; }

        /// <summary>
        /// Gets rules button rect
        /// </summary>
        public Rect RulesButton { get; }

        /// <summary>
        /// Gets exit button rect
        /// </summary>
        public Rect ExitButton { get; }

        /// <summary>
        /// Gets high score list background
        /// </summary>
        public Rect HighScoreListBackground { get; }

        /// <summary>
        /// Gets high score exit button rect
        /// </summary>
        public Rect HighScoreExitButton { get; }

        // High Scores / Rules

        /// <summary>
        /// Gets game rules
        /// </summary>
        public string GameRules { get; private set; }

        /// <summary>
        /// Gets or sets high scores list string for the menu
        /// </summary>
        public string HighScores { get; set; }

        /// <summary>
        /// Gets minimum high scores
        /// </summary>
        public int LowestHighScore { get; private set; }

        /// <summary>
        /// Gets high score list lenght
        /// </summary>
        public int HighScoreListCount { get; private set; }

        /// <summary>
        /// Gets or sets players name
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets players score;
        /// </summary>
        public int ActualScore { get; set; }

        /// <summary>
        /// save high scores
        /// </summary>
        /// <param name="newHighScore">new high score</param>
        public void SaveHighScore(int newHighScore)
        {
            List<PlayerData> playersList = new List<PlayerData>();
            playersList.Add(new PlayerData(this.PlayerName, newHighScore));

            // Code from msdn
            try
            {
                using (StreamReader sr = new StreamReader("..\\..\\Files\\highscores.txt", Encoding.UTF8))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] temp = line.Split(',');
                        playersList.Add(new PlayerData(temp[1], int.Parse(temp[2])));
                    }
                }
            }
            catch (Exception e)
            {
                // Let the user know what went wrong.
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            playersList.Sort();

            // using sample from msdn
            using (StreamWriter sw = new StreamWriter("..\\..\\Files\\highscores.txt", false, Encoding.UTF8))
            {
                int i = 1;

                foreach (var player in playersList)
                {
                    if (player.Score >= this.LowestHighScore || i <= 13)
                    {
                        sw.WriteLine(i.ToString() + "," + player.Name + "," + player.Score);
                        i++;
                    }
                }
            }

            this.PlayerName = string.Empty;
            this.LoadHighScores();
        }

        /// <summary>
        /// Get the highscores from file
        /// </summary>
        private void LoadHighScores()
        {
            string highscores = string.Empty;
            this.HighScoreListCount = 0;

            string[] highScores = File.ReadAllLines("..\\..\\Files\\highscores.txt", Encoding.UTF8);

            foreach (var item in highScores)
            {
                string[] temp = item.Split(',');
                highscores += temp[0] + ". " + temp[1];

                for (int i = 0; i < (33 - temp[2].Length - temp[1].Length); i++)
                {
                    highscores += ".";
                }

                highscores += temp[2] + " Points\n";
                this.HighScoreListCount++;
                if (this.HighScoreListCount == 13 || this.HighScoreListCount == temp.Length)
                {
                    this.LowestHighScore = int.Parse(temp[2]);
                }
            }

            this.HighScores = highscores;
        }

        /// <summary>
        /// Get rules
        /// </summary>
        private void LoadRules()
        {
            string[] temp = File.ReadAllLines("..\\..\\Files\\rules.txt", Encoding.Default);

            foreach (var item in temp)
            {
                this.GameRules += item + "\n";
            }
        }
    }
}
