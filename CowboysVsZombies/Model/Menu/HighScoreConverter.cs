﻿// <copyright file="HighScoreConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model.Menu
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using Model;
    using View;

    /// <summary>
    /// Convert high score int to string
    /// </summary>
    public class HighScoreConverter : IValueConverter
    {
        /// <summary>
        /// Convert int to string for xml
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>highscore string</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = string.Empty;
            int highscore = (int)value;

            s = "Your score is: " + highscore.ToString() + " Points";
            return s;
        }

        /// <summary>
        /// convertback
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targettype</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>nothing</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
