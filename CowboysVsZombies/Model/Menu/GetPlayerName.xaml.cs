﻿// <copyright file="GetPlayerName.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model.Menu
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using Model;

    /// <summary>
    /// Interaction logic for GetPlayerName.xaml
    /// </summary>
    public partial class GetPlayerName : Window
    {
        private MenuModel menuModel;
        private int newScore;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetPlayerName"/> class.
        /// </summary>
        /// <param name="menuModel">passes menumodel</param>
        /// <param name="newScore">new score</param>
        public GetPlayerName(MenuModel menuModel, int newScore)
        {
            this.InitializeComponent();
            this.menuModel = menuModel;
            this.newScore = newScore;
            this.DataContext = this.menuModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression be = this.tb_playername.GetBindingExpression(TextBox.TextProperty);
            be.UpdateSource();

            this.menuModel.SaveHighScore(this.newScore);

            this.Close();
        }

        /// <summary>
        /// Max 10 characters for name
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Tb_playername_LostFocus(object sender, RoutedEventArgs e)
        {
            string playerName = this.tb_playername.Text;
            int i = 0;

            foreach (char item in playerName)
            {
                i++;
            }

            if (playerName.Length <= 10)
            {
                this.lb_nameLenghtError.Visibility = Visibility.Hidden;
                this.bt_OK.Visibility = Visibility.Visible;
            }
            else
            {
                this.lb_nameLenghtError.Visibility = Visibility.Visible;
                this.bt_OK.Visibility = Visibility.Hidden;
            }
        }
    }
}
