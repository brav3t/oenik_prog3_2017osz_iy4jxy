﻿// <copyright file="PlayerData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model.Menu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// why!!!
    /// </summary>
    public class PlayerData : IComparable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerData"/> class.
        /// </summary>
        /// <param name="name">player name</param>
        /// <param name="score">player score</param>
        public PlayerData(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }

        /// <summary>
        /// Gets players name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets player score
        /// </summary>
        public int Score { get; private set; }

        /// <summary>
        /// Compares players
        /// </summary>
        /// <param name="obj">playerdata object</param>
        /// <returns>compare result </returns>
        public int CompareTo(object obj)
        {
            return (obj as PlayerData).Score.CompareTo(this.Score);
        }
    }
}
