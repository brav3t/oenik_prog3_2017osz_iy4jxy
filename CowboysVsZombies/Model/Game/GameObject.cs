﻿// <copyright file="GameObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Adds geometry to game objects
    /// </summary>
    public class GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// </summary>
        /// <param name="newGeometry">The other geometry</param>
        public GameObject(Geometry newGeometry)
        {
            this.GeometryObject = newGeometry;
        }

        /// <summary>
        /// Gets or sets actual geometry
        /// </summary>
        public Geometry GeometryObject { get; set; }

        /// <summary>
        /// Check if geometrys are collided
        /// </summary>
        /// <param name="otherGeometry">the other geometry</param>
        /// <returns>bool collide happened?</returns>
        public bool Collide(GameObject otherGeometry)
        {
            Geometry collision =
                Geometry.Combine(this.GeometryObject, otherGeometry.GeometryObject, GeometryCombineMode.Intersect, null);
            return collision.GetArea() > 0;
        }

        /// <summary>
        /// Clones the geometry
        /// </summary>
        /// <param name="newTransform">The transformation</param>
        protected void Transform(Transform newTransform)
        {
            Geometry clone = this.GeometryObject.Clone();
            clone.Transform = newTransform;
            this.GeometryObject = clone.GetFlattenedPathGeometry();
        }
    }
}
