﻿// <copyright file="GameEndArgs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Get who wins
    /// </summary>
    public class GameEndArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameEndArgs"/> class.
        /// </summary>
        /// <param name="who">who did win</param>
        public GameEndArgs(string who)
        {
            this.Who = who;
        }

        /// <summary>
        /// Gets who wins
        /// </summary>
        public string Who { get; private set; }
    }
}
