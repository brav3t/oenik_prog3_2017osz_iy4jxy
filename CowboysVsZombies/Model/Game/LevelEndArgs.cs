﻿// <copyright file="LevelEndArgs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Which level is complete
    /// </summary>
    public class LevelEndArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LevelEndArgs"/> class.
        /// </summary>
        /// <param name="s">level index</param>
        public LevelEndArgs(int s)
        {
            this.S = s;
        }

        /// <summary>
        /// Gets event parameter
        /// </summary>
        public int S { get; private set; }
    }
}
