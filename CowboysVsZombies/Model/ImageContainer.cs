﻿// <copyright file="ImageContainer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Hold the ImageBrushez
    /// </summary>
    public class ImageContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImageContainer"/> class.
        /// </summary>
        public ImageContainer()
        {
            // Images
            BitmapImage bitmapImage = new BitmapImage(new Uri("..\\..\\Files\\MainMenuBackground.png", UriKind.Relative));
            this.MenuBackground = new ImageBrush(bitmapImage);
            this.MenuBackground.Freeze();

            bitmapImage = new BitmapImage(new Uri("..\\..\\Files\\MapBackground.png", UriKind.Relative));
            this.MapBackground = new ImageBrush(bitmapImage);
            this.MapBackground.Freeze();

            // Colors
            BrushConverter bc = new BrushConverter();
            this.ZombieLightBrown = (SolidColorBrush)bc.ConvertFrom("#8c5b00");
            this.ZombieLightBrown.Freeze();

            this.ZombieDarkBrown = (SolidColorBrush)bc.ConvertFrom("#3b2110");
            this.ZombieLightBrown.Freeze();

            this.MenuOpaWhite = new SolidColorBrush(Colors.White);
            this.MenuOpaWhite.Opacity = 0.6;
            this.MenuOpaWhite.Freeze();

            // Pen
            this.MenuPen = new Pen(this.ZombieLightBrown, 2);
        }

        // Images

        /// <summary>
        /// Gets main menu background
        /// </summary>
        public ImageBrush MenuBackground { get; }

        /// <summary>
        /// Gets Mapbrush
        /// </summary>
        public ImageBrush MapBackground { get; }

        // Colors

        /// <summary>
        /// Gets customcolor
        /// </summary>
        public SolidColorBrush ZombieLightBrown { get; }

        /// <summary>
        /// Gets customcolor
        /// </summary>
        public SolidColorBrush ZombieDarkBrown { get; }

        /// <summary>
        /// Gets customcolor
        /// </summary>
        public SolidColorBrush MenuOpaWhite { get; }

        // Pen

        /// <summary>
        /// Gets game pen
        /// </summary>
        public Pen MenuPen { get; }

        // Gametext

        /// <summary>
        /// Formates to game format
        /// </summary>
        /// <param name="text">the text</param>
        /// <param name="size">its size</param>
        /// <param name="color">its color</param>
        /// <returns>Game like text</returns>
        public FormattedText GetGameText(string text, int size, SolidColorBrush color)
        {
            FormattedText ftext = new FormattedText(
                text,
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Copperplate Gothic Bold Regular"),
                size,
                color);

            return ftext;
        }
    }
}
