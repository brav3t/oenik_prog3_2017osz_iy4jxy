﻿// <copyright file="LevelStartAnimation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model.Animation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using VM;

    /// <summary>
    /// Level star animation
    /// </summary>
    public class LevelStartAnimation
    {
        private ImageContainer imageContainer;
        private GameLogic gameLogic;

        private int levelNumber;

        // private SolidColorBrush blackBackground;
        private Point backGroundPoint;
        private Size backGroundSize;
        private Rect backgroundRect;

        private Point levelTextPoint;
        private Point getReadyTextPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelStartAnimation"/> class.
        /// </summary>
        /// <param name="gameLogic">the game logic</param>
        /// <param name="imageContainer">the imagecontainer</param>
        public LevelStartAnimation(ImageContainer imageContainer, GameLogic gameLogic)
        {
            this.imageContainer = imageContainer;
            this.gameLogic = gameLogic;
            this.TickCounter = 0;

            this.backGroundPoint = new Point(0, 0);
            this.backGroundSize = new Size(650, 500);
            this.backgroundRect = new Rect(this.backGroundPoint, this.backGroundSize);

            this.levelTextPoint = new Point(265, 150);
            this.getReadyTextPoint = new Point(240, 205);
        }

        /// <summary>
        /// triggers animation end
        /// </summary>
        public event EventHandler AnimationEnded;

        /// <summary>
        /// Gets or sets tickcounter
        /// </summary>
        public int TickCounter { get; set; }

        /// <summary>
        /// generates animation
        /// </summary>
        /// <param name="drawingContext">drawingcontext in</param>
        public void Play(DrawingContext drawingContext)
        {
            string level = "Level " + this.levelNumber.ToString();
            this.levelNumber = this.gameLogic.ActualLevel;

            int frameDiff = 50;

            drawingContext.DrawRectangle(this.imageContainer.MapBackground, null, this.backgroundRect);

            drawingContext.DrawText(
                this.imageContainer.GetGameText(
                this.gameLogic.Money.ToString() + "$", 20, this.imageContainer.ZombieLightBrown),
                this.gameLogic.MapModel.MoneyPlace);

            drawingContext.DrawText(
                this.imageContainer.GetGameText(
                this.gameLogic.Score.ToString() + " Pt", 20, this.imageContainer.ZombieLightBrown),
                this.gameLogic.MapModel.ScorePlace);

            drawingContext.DrawText(
                this.imageContainer.GetGameText(
                "Level " + this.gameLogic.ActualLevel.ToString(), 20, this.imageContainer.ZombieLightBrown),
                this.gameLogic.MapModel.LevelPlace);

            if (this.TickCounter <= frameDiff * 1)
            {
                drawingContext.DrawText(this.imageContainer.GetGameText(level, 40, new SolidColorBrush(Colors.White)), this.levelTextPoint);
            }
            else if (this.TickCounter > (frameDiff * 1) && this.TickCounter <= frameDiff * 2)
            {
                drawingContext.DrawText(this.imageContainer.GetGameText("Get Ready!", 40, new SolidColorBrush(Colors.White)), this.getReadyTextPoint);
            }
            else if (this.TickCounter > (frameDiff * 2))
            {
                this.AnimationEnded?.Invoke(this, new EventArgs());
                this.TickCounter = 0;
            }

            this.TickCounter++;
        }
    }
}
