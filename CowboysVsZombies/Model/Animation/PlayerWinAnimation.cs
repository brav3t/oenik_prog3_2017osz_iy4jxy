﻿// <copyright file="PlayerWinAnimation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model.Animation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// If player wins animation
    /// </summary>
    public class PlayerWinAnimation
    {
        private ImageContainer imageContainer;

        private int tickCounter;

        // private SolidColorBrush blackBackground;
        private Point backGroundPoint;
        private Size backGroundSize;
        private Rect backgroundRect;

        private ImageBrush[] cowGrildShootImages;
        private ImageBrush[] zombieDeadImages;

        private Point cowGirlPlace;
        private Point zombiePlace;
        private Size cowGirlSize;
        private Size zombieSize;

        private Rect cowGirlRect;
        private Rect zombieRect;

        private Point cowGirlSpeachPoint;
        private Point zombieSpeakPoint;

        private Point congratTextPoint;
        private Point defeatedTextPoint;
        private Point enjoyWhiskeyTextPoint1;
        private Point enjoyWhiskeyTextPoint2;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerWinAnimation"/> class.
        /// </summary>
        /// <param name="imageContainer">gets imagecontainer</param>
        public PlayerWinAnimation(ImageContainer imageContainer)
        {
            this.imageContainer = imageContainer;
            this.tickCounter = 0;

            // this.blackBackground = new SolidColorBrush(Colors.Black);
            this.backGroundPoint = new Point(0, 0);
            this.backGroundSize = new Size(650, 500);
            this.backgroundRect = new Rect(this.backGroundPoint, this.backGroundSize);

            this.cowGirlPlace = new Point(70, 189);
            this.zombiePlace = new Point(290, 206);
            this.cowGirlSize = new Size(250, 211);
            this.zombieSize = new Size(250, 209);

            this.cowGirlRect = new Rect(this.cowGirlPlace, this.cowGirlSize);
            this.zombieRect = new Rect(this.zombiePlace, this.zombieSize);

            this.cowGirlSpeachPoint = new Point(150, 140);
            this.zombieSpeakPoint = new Point(290, 180);

            this.LoadImages();

            this.congratTextPoint = new Point(100, 30);
            this.defeatedTextPoint = new Point(15, 40);
            this.enjoyWhiskeyTextPoint1 = new Point(30, 20);
            this.enjoyWhiskeyTextPoint2 = new Point(30, 60);
    }

        /// <summary>
        /// triggers animation end
        /// </summary>
        public event EventHandler AnimationEnded;

        /// <summary>
        /// generates animation
        /// </summary>
        /// <param name="drawingContext">drawingcontext in</param>
        public void Play(DrawingContext drawingContext)
        {
            int frameDiff = 4;

            drawingContext.DrawRectangle(this.imageContainer.MenuBackground, null, this.backgroundRect);

            if (this.tickCounter <= frameDiff * 1)
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[0], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 1) && this.tickCounter <= frameDiff * 2)
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[0], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 2) && this.tickCounter <= (frameDiff * 3))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[0], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[2], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 3) && this.tickCounter <= (frameDiff * 4))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[0], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[2], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 4) && this.tickCounter <= (frameDiff * 5))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[1], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[0], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 5) && this.tickCounter <= (frameDiff * 6))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[3], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[0], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
            }
            else if (this.tickCounter > (frameDiff * 6) && this.tickCounter <= (frameDiff * 7))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[4], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 7) && this.tickCounter <= (frameDiff * 8))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[5], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 8) && this.tickCounter <= (frameDiff * 9))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[6], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 9) && this.tickCounter <= (frameDiff * 10))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[7], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 10) && this.tickCounter <= (frameDiff * 11))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[8], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 11) && this.tickCounter <= (frameDiff * 12))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[9], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 12) && this.tickCounter <= (frameDiff * 13))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 13) && this.tickCounter <= (frameDiff * 14))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[11], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 14) && this.tickCounter <= (frameDiff * 15))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 15) && this.tickCounter <= (frameDiff * 16))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[11], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Dodge this!", 30, this.imageContainer.ZombieLightBrown), this.cowGirlSpeachPoint);
                drawingContext.DrawText(this.imageContainer.GetGameText("Awww..", 30, this.imageContainer.ZombieLightBrown), this.zombieSpeakPoint);
            }
            else if (this.tickCounter > (frameDiff * 16) && this.tickCounter <= (frameDiff * 17))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);
            }
            else if (this.tickCounter > (frameDiff * 16) && this.tickCounter <= (frameDiff * 46))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Congratulations!", 45, this.imageContainer.ZombieLightBrown), this.congratTextPoint);
            }
            else if (this.tickCounter > (frameDiff * 46) && this.tickCounter <= (frameDiff * 76))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("You have defeated the zombie horde!", 30, this.imageContainer.ZombieLightBrown), this.defeatedTextPoint);
            }
            else if (this.tickCounter > (frameDiff * 76) && this.tickCounter <= (frameDiff * 106))
            {
                drawingContext.DrawRectangle(this.zombieDeadImages[10], null, this.zombieRect);
                drawingContext.DrawRectangle(this.cowGrildShootImages[1], null, this.cowGirlRect);

                drawingContext.DrawText(this.imageContainer.GetGameText("Now you can enjoy", 30, this.imageContainer.ZombieLightBrown), this.enjoyWhiskeyTextPoint1);
                drawingContext.DrawText(this.imageContainer.GetGameText("your whiskey in the saloon!", 30, this.imageContainer.ZombieLightBrown), this.enjoyWhiskeyTextPoint2);
            }
            else if (this.tickCounter > (frameDiff * 106))
            {
                this.AnimationEnded?.Invoke(this, new EventArgs());
                this.tickCounter = 0;
            }

            this.tickCounter++;
        }

        private void LoadImages()
        {
            BitmapImage bmi;

            this.cowGrildShootImages = new ImageBrush[3];
            this.zombieDeadImages = new ImageBrush[12];

            string s1 = "..\\..\\Files\\Animation\\CowGirlShoot\\CowGirlShoot";
            string s2 = ".png";
            string uri = string.Empty;

            for (int i = 0; i < this.cowGrildShootImages.Length; i++)
            {
                uri = s1 + (i + 1).ToString() + s2;
                bmi = new BitmapImage(new Uri(uri, UriKind.Relative));
                this.cowGrildShootImages[i] = new ImageBrush(bmi);
                this.cowGrildShootImages[i].Freeze();
            }

            s1 = "..\\..\\Files\\Animation\\ZombieDies\\ZombieDead";

            for (int i = 0; i < this.zombieDeadImages.Length; i++)
            {
                uri = s1 + (i + 1).ToString() + s2;
                bmi = new BitmapImage(new Uri(uri, UriKind.Relative));
                this.zombieDeadImages[i] = new ImageBrush(bmi);
                this.zombieDeadImages[i].Freeze();
            }
        }
    }
}
