﻿// <copyright file="MapModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Game map's model
    /// </summary>
    public class MapModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapModel"/> class.
        /// </summary>
        public MapModel()
        {
            this.CreateSaloon();
            this.MoneyPlace = new Point(205, 70);
            this.ScorePlace = new Point(205, 40);
            this.LevelPlace = new Point(205, 10);

            Point clickedCowgirlPlace = new Point(3, 3);
            Point clickedCactusPlace = new Point(103, 3);
            Size clickedSize = new Size(94, 94);
            this.ClickedCowgirlPlace = new Rect(clickedCowgirlPlace, clickedSize);
            this.ClickedCactusPlace = new Rect(clickedCactusPlace, clickedSize);

            BrushConverter bc = new BrushConverter();
            SolidColorBrush zombieLightBrown = (SolidColorBrush)bc.ConvertFrom("#8c5b00");
            zombieLightBrown.Freeze();
            this.Frame = new Pen(zombieLightBrown, 2);

            this.InitializeFields();
        }

        /// <summary>
        /// Gets Click frame
        /// </summary>
        public Pen Frame { get; private set; }

        /// <summary>
        /// Gets MapBrush
        /// </summary>
        public Brush MapBrush { get; }

        /// <summary>
        /// Gets where the money will be written
        /// </summary>
        public Point MoneyPlace { get; }

        /// <summary>
        /// Gets where the score will be written
        /// </summary>
        public Point ScorePlace { get; }

        /// <summary>
        /// Gets where the level will be written
        /// </summary>
        public Point LevelPlace { get; }

        /// <summary>
        /// Gets where cowgirl image is
        /// </summary>
        public Rect ClickedCowgirlPlace { get; }

        /// <summary>
        /// Gets where cowgirl image is
        /// </summary>
        public Rect ClickedCactusPlace { get; }

        /// <summary>
        /// Gets the sallon geometry
        /// </summary>
        public GameObject TheSaloon { get; private set; }

        /// <summary>
        /// Gets the maps fields
        /// </summary>
        public MapField[] Fields { get; private set; }

        /// <summary>
        /// Reset the fields for new level
        /// </summary>
        public void ResetFields()
        {
            for (int i = 0; i < this.Fields.Length; i++)
            {
                this.Fields[i].FieldIsAvailable = true;
            }
        }

        /// <summary>
        /// Creates a geometry for the saloon
        /// </summary>
        private void CreateSaloon()
        {
            Point saloonLocation = new Point(0, 100);
            Size saloonSize = new Size(50, 400);
            Rect saloonRect = new Rect(saloonLocation, saloonSize);
            RectangleGeometry saloon = new RectangleGeometry(saloonRect);
            this.TheSaloon = new GameObject(saloon);
        }

        /// <summary>
        /// Initializes the fields
        /// </summary>
        private void InitializeFields()
        {
            this.Fields = new MapField[20];

            for (int i = 0; i < this.Fields.Length; i++)
            {
                this.Fields[i] = new MapField();
                this.Fields[i].FieldIsAvailable = true;
            }

            this.Fields[0].PlacePoint = new Point(55, 105);
            this.Fields[1].PlacePoint = new Point(55, 205);
            this.Fields[2].PlacePoint = new Point(55, 305);
            this.Fields[3].PlacePoint = new Point(55, 405);
            this.Fields[4].PlacePoint = new Point(155, 105);
            this.Fields[5].PlacePoint = new Point(155, 205);
            this.Fields[6].PlacePoint = new Point(155, 305);
            this.Fields[7].PlacePoint = new Point(155, 405);
            this.Fields[8].PlacePoint = new Point(255, 105);
            this.Fields[9].PlacePoint = new Point(255, 205);
            this.Fields[10].PlacePoint = new Point(255, 305);
            this.Fields[11].PlacePoint = new Point(255, 405);
            this.Fields[12].PlacePoint = new Point(355, 105);
            this.Fields[13].PlacePoint = new Point(355, 205);
            this.Fields[14].PlacePoint = new Point(355, 305);
            this.Fields[15].PlacePoint = new Point(355, 405);
            this.Fields[16].PlacePoint = new Point(455, 105);
            this.Fields[17].PlacePoint = new Point(455, 205);
            this.Fields[18].PlacePoint = new Point(455, 305);
            this.Fields[19].PlacePoint = new Point(455, 405);
        }
    }
}
