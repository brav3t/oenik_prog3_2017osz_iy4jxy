﻿// <copyright file="MapField.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System.Windows;

    /// <summary>
    /// fdas
    /// </summary>
    public class MapField
    {
        /// <summary>
        /// Gets or sets a value indicating whether this is is avaiable
        /// </summary>
        public bool FieldIsAvailable { get; set; }

        /// <summary>
        /// Gets or sets where to place the character
        /// </summary>
        public Point PlacePoint { get; set; }
    }
}
