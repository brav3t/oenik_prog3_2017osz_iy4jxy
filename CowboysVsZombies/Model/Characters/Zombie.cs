﻿// <copyright file="Zombie.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// The zombies
    /// </summary>
    internal class Zombie : Character
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Zombie"/> class.
        /// </summary>
        /// <param name="attackPower">attackpower</param>
        /// <param name="characterState">state</param>
        /// <param name="healthPoint">health</param>
        /// <param name="characterGeometry">characters geometry</param>
        public Zombie(Geometry characterGeometry, CharacterStates characterState, int healthPoint, int attackPower)
            : base(characterGeometry, characterState, healthPoint, attackPower)
        {
            // BitmapImage zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalk.png", UriKind.Relative));
            // BitmapImage zombieAttack = new BitmapImage(new Uri("..\\..\\Files\\ZombieAttack.png", UriKind.Relative));
            // this.WalkBrush = new ImageBrush(zombieWalk);
            // this.AttackBrush = new ImageBrush(zombieAttack);
            // this.WalkBrush.Freeze();
            // this.AttackBrush.Freeze();
            this.DefaultCharState = characterState;

            this.WalkBrushLoader();
            this.AttackBrushLoader();
        }

        private void WalkBrushLoader()
        {
            BitmapImage zombieWalk;
            this.WalkBrushAnimated = new ImageBrush[50];

            for (int i = 0; i < this.WalkBrushAnimated.Length; i++)
            {
                int index = 1;

                if (i < 5)
                {
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 5 && i < 10)
                {
                    index = 2;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 10 && i < 15)
                {
                    index = 3;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 15 && i < 20)
                {
                    index = 4;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 20 && i < 25)
                {
                    index = 5;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 25 && i < 30)
                {
                    index = 6;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 30 && i < 35)
                {
                    index = 7;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 35 && i < 40)
                {
                    index = 8;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 40 && i < 45)
                {
                    index = 9;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
                else if (i >= 45 && i < 50)
                {
                    index = 10;
                    zombieWalk = new BitmapImage(new Uri("..\\..\\Files\\ZombieWalkImages\\ZombieWalk" + index.ToString() + ".png", UriKind.Relative));
                    this.WalkBrushAnimated[i] = new ImageBrush(zombieWalk);
                    this.WalkBrushAnimated[i].Freeze();
                }
            }
        }

        private void AttackBrushLoader()
        {
            BitmapImage zombieAttack;
            this.AttackBrushAnimated = new ImageBrush[50];

            for (int i = 0; i < this.AttackBrushAnimated.Length; i++)
            {
                zombieAttack = new BitmapImage(new Uri("..\\..\\Files\\ZombieAttack.png", UriKind.Relative));
                this.AttackBrushAnimated[i] = new ImageBrush(zombieAttack);
                this.AttackBrushAnimated[i].Freeze();
            }
        }
    }
}
