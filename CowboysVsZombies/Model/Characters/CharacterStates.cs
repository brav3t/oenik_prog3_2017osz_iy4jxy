﻿// <copyright file="CharacterStates.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    /// <summary>
    /// The possible states of the game characters
    /// </summary>
    public enum CharacterStates
    {
        /// <summary>
        /// Repesents the Idle character state
        /// </summary>
        Idle,

        /// <summary>
        /// Repesents the Moving character state
        /// </summary>
        Moving,

        /// <summary>
        /// Repesents the Attacking character state
        /// </summary>
        Attacking
    }
}
