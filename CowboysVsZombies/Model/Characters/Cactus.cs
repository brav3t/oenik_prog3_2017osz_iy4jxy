﻿// <copyright file="Cactus.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// the cactus class
    /// </summary>
    public class Cactus : Character
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cactus"/> class.
        /// </summary>
        /// <param name="characterGeometry">char geom</param>
        /// <param name="characterState">char state</param>
        /// <param name="healthPoint">char hp</param>
        /// <param name="attackPower">char attackpower</param>
        public Cactus(Geometry characterGeometry, CharacterStates characterState, int healthPoint, int attackPower)
            : base(characterGeometry, characterState, healthPoint, attackPower)
        {
            BitmapImage cactusIdle = new BitmapImage(new Uri("..\\..\\Files\\Cactus.png", UriKind.Relative));
            this.IdleBrush = new ImageBrush(cactusIdle);
            this.IdleBrush.Freeze();

            // this.AttackBrush = new ImageBrush(cactusIdle);
            // this.AttackBrush.Freeze();
            this.DefaultCharState = characterState;

            this.AttackBrushLoader();
        }

        private void AttackBrushLoader()
        {
            BitmapImage cactusAttack;
            this.AttackBrushAnimated = new ImageBrush[50];

            for (int i = 0; i < this.AttackBrushAnimated.Length; i++)
            {
                cactusAttack = new BitmapImage(new Uri("..\\..\\Files\\Cactus.png", UriKind.Relative));
                this.AttackBrushAnimated[i] = new ImageBrush(cactusAttack);
                this.AttackBrushAnimated[i].Freeze();
            }
        }
    }
}
