﻿// <copyright file="CowGirl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// The Cowgirld character
    /// </summary>
    public class CowGirl : Character
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CowGirl"/> class.
        /// </summary>
        /// <param name="characterGeometry">girls geometry</param>
        /// <param name="characterState">girls states</param>
        /// <param name="healthPoint">girls health</param>
        /// <param name="attackPower">girls attackpower</param>
        public CowGirl(Geometry characterGeometry, CharacterStates characterState, int healthPoint, int attackPower)
            : base(characterGeometry, characterState, healthPoint, attackPower)
        {
            // BitmapImage cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowgirlAttack.png", UriKind.Relative));
            BitmapImage cowgirlIdle = new BitmapImage(new Uri("..\\..\\Files\\CowgirlIdle.png", UriKind.Relative));

            // this.AttackBrush = new ImageBrush(cowgirlAttack);
            this.IdleBrush = new ImageBrush(cowgirlIdle);

            // this.AttackBrush.Freeze();
             this.IdleBrush.Freeze();
            this.DefaultCharState = characterState;

            this.AttackBrushLoader();
        }

        private void AttackBrushLoader()
        {
            BitmapImage cowgirlAttack;
            this.AttackBrushAnimated = new ImageBrush[50];

            for (int i = 0; i < this.AttackBrushAnimated.Length; i++)
            {
                int index = 1;

                if (i < 8)
                {
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
                else if (i >= 8 && i < 16)
                {
                    index = 2;
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
                else if (i >= 16 && i < 25)
                {
                    index = 3;
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
                else if (i >= 25 && i < 32)
                {
                    index = 1;
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
                else if (i >= 32 && i < 41)
                {
                    index = 2;
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
                else if (i >= 41 && i < 50)
                {
                    index = 3;
                    cowgirlAttack = new BitmapImage(new Uri("..\\..\\Files\\CowGirlAttackImages\\CowgirlAttack" + index.ToString() + ".png", UriKind.Relative));
                    this.AttackBrushAnimated[i] = new ImageBrush(cowgirlAttack);
                    this.AttackBrushAnimated[i].Freeze();
                }
            }
        }
    }
}