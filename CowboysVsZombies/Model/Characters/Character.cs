﻿// <copyright file="Character.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Defining the common attributes and methods for the possible characters
    /// </summary>
    public abstract class Character : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Character"/> class.
        /// </summary>
        /// <param name="characterState">provides the characters state</param>
        /// <param name="healthPoint">providess the health point</param>
        /// <param name="attackPower">provides the attackpower</param>
        /// <param name="characterGeometry">characters geometry</param>
        public Character(Geometry characterGeometry, CharacterStates characterState, int healthPoint, int attackPower)
            : base(characterGeometry)
        {
            this.CharState = characterState;
            this.HealthPoint = healthPoint;
            this.AttackPower = attackPower;

            this.WalkAnimEnumerator = 48;
            this.AttackAnimEnumerator = 48;
        }

        /// <summary>
        /// Gets or sets the characters actual state
        /// </summary>
        public CharacterStates CharState { get; set; }

        /// <summary>
        /// Gets or sets characters attack target
        /// </summary>
        public Character Target { get; set; }

        /// <summary>
        /// Gets or sets the character healt
        /// </summary>
        public int HealthPoint { get; set; }

        /// <summary>
        /// Gets or sets the character's field
        /// </summary>
        public MapField CharacterField { get; set; }

        /// <summary>
        /// Gets or sets characters default state
        /// </summary>
        protected CharacterStates DefaultCharState { get; set; }

        /// <summary>
        /// Gets or sets the character actual brush
        /// </summary>
        protected Brush ActualBrush { get; set; }

        /// <summary>
        /// Gets or sets walkBrush
        /// </summary>
        protected ImageBrush WalkBrush { get; set; }

        /// <summary>
        /// Gets or sets attackBrush
        /// </summary>
        protected ImageBrush AttackBrush { get; set; }

        /// <summary>
        /// Gets or sets idleBrush
        /// </summary>
        protected ImageBrush IdleBrush { get; set; }

        /// <summary>
        /// Gets or sets the characters attackpower
        /// </summary>
        protected int AttackPower { get; set; }

        /// <summary>
        /// Gets or sets walk animation
        /// </summary>
        protected ImageBrush[] WalkBrushAnimated { get; set; }

        /// <summary>
        /// Gets or sets attack animation
        /// </summary>
        protected ImageBrush[] AttackBrushAnimated { get; set; }

        /// <summary>
        /// Gets or sets idle animation
        /// </summary>
        protected ImageBrush[] IdleBrushAnimated { get; set; }

        /// <summary>
        /// Gets or sets the cycle through the animation
        /// </summary>
        private int WalkAnimEnumerator { get; set; }

        /// <summary>
        /// Gets or sets the cycle through the animation
        /// </summary>
        private int AttackAnimEnumerator { get; set; }

        /// <summary>
        /// The char acts as its state
        /// </summary>
        public void Act()
        {
            switch (this.CharState)
            {
                case CharacterStates.Idle:
                    break;
                case CharacterStates.Moving:
                    this.Move();
                    break;
                case CharacterStates.Attacking:
                    this.Attack();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Get charackters brush
        /// </summary>
        /// <returns>Characters brush</returns>
        public Brush GetBrush()
        {
            switch (this.CharState)
            {
                case CharacterStates.Idle:
                    return this.IdleBrush;
                case CharacterStates.Moving:
                    return this.GetWalkBrush();
                case CharacterStates.Attacking:
                    return this.GetAttackBrush();
                default:
                    return this.ActualBrush;
            }
        }

        /// <summary>
        /// Character attacks
        /// </summary>
        protected virtual void Attack()
        {
            if (this.Target.HealthPoint > 0)
            {
                this.Target.HealthPoint -= this.AttackPower;
            }
            else if (this.Target.HealthPoint <= 0)
            {
                this.CharState = this.DefaultCharState;
                this.Target = null;
            }
        }

        /// <summary>
        /// Character moves
        /// </summary>
        protected virtual void Move()
        {
            TranslateTransform move = new TranslateTransform(-1, 0);
            this.Transform(move);
        }

        private Brush GetWalkBrush()
        {
            this.WalkAnimEnumerator++;
            if (this.WalkAnimEnumerator == 49)
            {
                this.WalkAnimEnumerator = 0;
            }

            return this.WalkBrushAnimated[this.WalkAnimEnumerator];
        }

        private Brush GetAttackBrush()
        {
            this.AttackAnimEnumerator++;
            if (this.AttackAnimEnumerator == 49)
            {
                this.AttackAnimEnumerator = 0;
            }

            return this.AttackBrushAnimated[this.AttackAnimEnumerator];
        }
    }
}
