﻿// <copyright file="CharacterTypes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CowboysVsZombies.Model
{
    /// <summary>
    /// Possible game charaters for placing
    /// </summary>
    public enum CharacterTypes
    {
        /// <summary>
        /// Cowgirl
        /// </summary>
        CowGirl,

        /// <summary>
        /// Cactus
        /// </summary>
        Cactus,

        /// <summary>
        /// Zombie just for the list not used
        /// </summary>
        Zombie,

        /// <summary>
        /// Bullet just for the list not used
        /// </summary>
        Bullet
    }
}
